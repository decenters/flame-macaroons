{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DataKinds #-}
module Flame.Macaroons
       ( Macaroon
       , IFCVerifier
       , ReturnCode(..)
       , MacaroonFormat(..)
       , create
       , validate
       , location
       , identifier
       , signature
       , copy
       , serialize
       , inspect
       , deserialize
       , thirdPartyCaveats
       , prepareForRequest
       , addFirstPartyCaveat
       , addThirdPartyCaveat
       , verifierCreate
       , verify
       , satisfyExact
       , satisfyGeneral
       , unsafeUnwrap
       )
where 

import Flame.Principals
import Flame.TCB.IFC

import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal.Utils

import Data.ByteString.Char8 (ByteString, pack, empty)

import Macaroons hiding (verifierCreate, verify, satisfyExact, satisfyGeneral)
import Macaroons.Bindings 
import qualified Macaroons as M

newtype IFCVerifier (pc::KPrin) (l::KPrin) =
  IFCVerifier { unsafeUnwrap :: ForeignPtr VerifierStruct }

-- | Create a verifier bound by principal l
verifierCreate :: IFC m IO n => SPrin pc
               -> SPrin l
               -> m IO n pc' pc' (IFCVerifier pc l)
verifierCreate pc l = unsafeProtect $ do v <- M.verifierCreate
                                         return $ label $ IFCVerifier v

-- | Verify a macaroon.  TODO: what about the secret?
-- NB: there is a channel created from knowing the macaroon in hand
-- is derived from one created with the secret? This API implicitly
-- declassifies this information 
verify :: (IFC m IO n, pc ⊑ pc') =>
       IFCVerifier pc' l
       -> Macaroon
       -> Lbl l' ByteString
       -> [Macaroon]
       -> m IO n pc l (Either ReturnCode Bool)
verify v m secret dms = 
  unsafeProtect $ do r <- M.verify v' m secret' dms
                     return $ label r
  where
    v' = unsafeUnwrap v
    secret' = unsafeUnlabel secret

-- | Add a predicate to the verifier.
--   Since this imperatively updates the verifier, the current IFC context must be bound by l.
satisfyExact :: (IFC m IO n, pc ⊑ l) => IFCVerifier pc' l -> ByteString -> m IO n pc l (Either ReturnCode Bool)
satisfyExact v pred = unsafeProtect $ do r <- M.satisfyExact v' pred
                                         return $ label r
  where
    v' = unsafeUnwrap v

-- | Add a predicate funtion to the verifier.
--   Since this imperatively updates the verifier, the current IFC context must be bound by l.
satisfyGeneral :: (IFC m IO n, pc ⊑ l) =>
                  IFCVerifier pc' l
                  -> (String -> m IO n pc' l Bool)
                  -> m IO n pc l (Either ReturnCode Bool)
satisfyGeneral v f = unsafeProtect $ do r <- M.satisfyGeneral v' f'
                                        return $ label r
  where
    v' = unsafeUnwrap v
    f' caveat = do res <- runIFC $ f caveat
                   return $ unsafeUnlabel res